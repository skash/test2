#!/usr/bin/env bash

# FIX TO CONVERT MAT FROM HEX TO DEC

export LC_ALL=C

infile="${1:-/dev/stdin}"

while read line; do

    for number in $line; do
        printf "%f " "$number"
    done
    echo
done < $infile
